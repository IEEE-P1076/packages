library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.numeric_bit.all;

package test_support_pkg is
  function is_null_range(value : std_logic_vector) return boolean;
  function is_null_range(value : bit_vector) return boolean;
  function is_null_range(value : ieee.numeric_std.unsigned) return boolean;
  function is_null_range(value : ieee.numeric_std.signed) return boolean;
  function is_null_range(value : ieee.numeric_bit.unsigned) return boolean;
  function is_null_range(value : ieee.numeric_bit.signed) return boolean;
end package;

package body test_support_pkg is
  function is_null_range(value : std_logic_vector) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

  function is_null_range(value : bit_vector) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

  function is_null_range(value : ieee.numeric_std.unsigned) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

  function is_null_range(value : ieee.numeric_std.signed) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

  function is_null_range(value : ieee.numeric_bit.unsigned) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

  function is_null_range(value : ieee.numeric_bit.signed) return boolean is
    constant result : boolean := value'length = 0;
  begin
    return result;
  end;

end package body;
