# VHPI UML Model

This directory contains the VHPI information model
using the Unified Modeling Language (UML)
as described in Clause 19 of IEEE 1076-2019.

The model conforms to UML 2.5.

## Viewing the Model

The model is contained in a project
for the open source modeling tool [Eclipse Papyrus](https://www.eclipse.org/papyrus/).

The project has been tested with:
- Papyrus 4.8 (Java 1.8 or higher)
- Papyrus 5 (Java 11 or higher)

To view the model and diagrams,
create or open a Papyrus workspace
then import this project into the workspace.
After import, open the VHPI project.

## Operating System Considerations

Papyrus uses ascii Line Feed (Lf) characters for multi-line comments,
whereas the original MagicDraw model used the escape code `&#10;`.

On Windows, Papyrus is inserting `&#xD;` before linebreaks in comments,
which is an escaped ascii Carriage Return (Cr).
When editing multi-line strings in Papyrus on Windows,
the Carriage Returns should be removed after saving
and before committing changes.

## Files

In addition to Papyrus project files, the following are of note:
- VHPI.uml contains the UML model in XMI format.
- VHPI.notation contains diagrams matching IEEE 1076-2019 in a Papyrus compatible format.

## Exporting Diagrams

With the Model open in Papyrus,
right click on the top-level Data (Model)
in the Model Explorer window,
then select Export > Export All Diagrams...

In the pop-up dialog window,
choose the desired output format
and then click the OK button.

The diagrams will be exported in the chosen format
and a message will display in a pop-up window.
