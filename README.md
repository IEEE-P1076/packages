# IEEE 1076: VHDL Packages

This repository contains open source materials referenced by the IEEE 1076 standard.
It was first published in December 2019, together with the release of [1076-2019](https://ieeexplore.ieee.org/document/8938196).
See tagged commits for states of the sources matching the LRM releases.

## License

All source code files in this repository are subject to the following copyright and licensing terms:

> Copyright 2019 IEEE P1076 WG Authors
>
> See the LICENSE file distributed with this work for copyright and licensing information, the AUTHORS file for a list
> of copyright holders, and the CONTRIBUTORS file for the list of contributors.

## Disclaimer

This open source repository contains material that may be included-in or referenced by an unapproved draft of a proposed
IEEE Standard.
All material in this repository is subject to change.
The material in this repository is presented "as is" and with all faults.
Use of the material is at the sole risk of the user.
IEEE specifically disclaims all warranties and representations with respect to all material contained in this repository
and shall not be liable, under any theory, for any use of the material.
Unapproved drafts of proposed IEEE standards must not be utilized for any conformance/compliance purposes.

# IEEE P1076 Working Group

Updates to the IEEE Std. 1076 and to these Packages are coordinated through the VHDL Analysis and Standardization Group
(C/DA/P1076/1076 aka. VASG).
Check the work in progress through the references below:

<p align="center">
  <a title="VHDL Analysis and Standardization Group (VASG)" href="https://IEEE-P1076.gitlab.io"><img src="https://img.shields.io/website.svg?label=IEEE-P1076.gitlab.io&longCache=true&style=flat-square&url=http%3A%2F%2FIEEE-P1076.gitlab.io%2Findex.html&logo=GitLab&logoColor=fff"></a><!--
  -->
  <a title="E-mail reflector/list" href="http://grouper.ieee.org/groups/1076/email"><img alt="E-mail reflector/list" src="https://img.shields.io/badge/-grouper.ieee.org/groups/1076-323131.svg?logo=ieee&style=flat-square&longCache=true"></a><!--
  -->
  <a title="EDA TWIKI P1076" href="http://www.eda-twiki.org/cgi-bin/view.cgi/P1076"><img alt="EDA TWIKI P1076" src="https://img.shields.io/badge/-eda--twiki.org-323131.svg?logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA%2FwD%2FAP%2BgvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QIWAhImtwMY5AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACwUlEQVRYw%2B1XTWgTQRT%2BZpOokURDg6GiaAwoCEX8QQpevFTQHgTBY8FrERSstKLePVSleLSH3qUFRSx48CR4CIJU8BC0WKqVULQiJsZmZ94%2BD83GJPOTDR68dGHZXYb3vu99882bWWDz%2Bs%2BXCF%2F408Au%2BD9GoaqXoIICFAQUAAWAGk%2FXTQCkAFSwhiAxD94%2BJS6vLEQiwOULp1B5Og9JGS2xbCEgDcDheOcYxQhb%2BibF2NdbTgL8%2BWQOv98sQlJaSyQ7qpeW6k0kQkWyR86Jq2%2Bf2wh4qK%2BMdgWXBnBpeaoWMpKB1dIztwKl9BL8Sl4DF6lXkMGyltT%2BruCrs%2FD9nJYre%2FCMuP3hhYlAHH5tvz5%2FAJL5B%2BL0u9leHM13My8h%2FZyWq1ot2KdAsdACFAAVxHpeUzZ%2F1Nc9BwHD3IXf%2F0og9A4Ja0jcCC4B8PfjPJuqNsfFtkUx8q3UlJvZw730eRBUI2YdvuzTTBrmdRIggwfqq%2BNQGG%2BuBtAkgBstsTHUfz1ur5j1FSEBJBBBAW2Nc8d0cKBFh4BdO6SLADk6XOs392A61YsCUcBVD6YzKeAwtOfs%2FW1O7kJAWjYtW6zmAVcFBCBwEJBdPKCimpAcVQkLARlxu3YSkBZg6pLAthWbVHWakAwHj04fmJopORSI2FXNCpBBjaBHBWQEBTf6gAdQ4PaATQFlUa6TmE9sJ4DkEmTlgBOcABD385XUMZAQUGBcP5To2v3CnIE3DOAhXxwUYq7I7QeS6d038bN8xwm%2B0QsYBO4Y89o2Hd%2BigAQwMDgl5opjeiOKZ6fBW9esFfxtRAISXgPUawO3kW7N97p4jQ%2FvfMQjw%2Fv0U%2FHM0X6U339BreZZk5gAQol9B7huygB7CvfFwseJ9v%2BCmaEMlosTUBhCfMcJqJY2TQ7DKccyNHVUBcAPgESSsDf%2FZPPXbPP6A13nhms8bYukAAAAAElFTkSuQmCC&style=flat-square&longCache=true"></a><!--
  -->
</p>
